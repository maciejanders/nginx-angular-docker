- basic info

  image is available on docker hub: https://hub.docker.com/r/maciejanders/nginx-angular/

  Openshift adjusted version: https://hub.docker.com/r/maciejanders/nginx-angular-os/ (Dockerfile - openshift folder)

  they are intended to be used as base images for angular js project deployments

  enjoy!

- extra notes

  Openshift adjusted image listens on port 8081, (default nginx image listens on port 80)
