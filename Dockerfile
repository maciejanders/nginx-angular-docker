FROM nginx:1.15.1-alpine
## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
# install node
RUN apk update
RUN apk upgrade
RUN apk add nodejs=8.9.3-r1
RUN npm -g install @angular/cli
#CMD ["nginx", "-g", "daemon off;"]
